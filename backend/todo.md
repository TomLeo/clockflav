# TODO

## Dev Ops

- [ ] Seperate configration secrets
- [ ] Add production database
- [ ] Configure gunicorn and nginx logic
- [ ] Deploy to server

### Seperate configration secrets
- Look into https://github.com/joke2k/django-environ

## Authentication

- [ ] Add Login Flow
- [ ] Store token in `localstorage`

## Permissions

Share time spent but not estimate? How granular should permissions be?

- [ ] [Add Object Level Permissions](https://github.com/django-guardian/django-guardian)
- [ ] Only show tasks for logged in user
- [ ] User share permissions, ability to share tasks on a per user basis
- [ ] User share permissions, ability to share task progress on a per user basis
- [ ] Group share permissions, ability to share tasks on a per group basis
- [ ] Group share permissions, ability to share task progress on a per group basis



