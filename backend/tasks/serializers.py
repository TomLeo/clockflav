from rest_framework import serializers
from tasks.models import Task, TaskProgress

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        # fields = ('user', 'name', 'slug', 'git_branch', 'pull_request_url', 'merge_status', 'time_estimate', 'complete')
        fields = '__all__'

class TaskProgressSerializer(serializers.ModelSerializer):
    # task = TaskSerializer()
    class Meta:
        model = TaskProgress
        # fields = ('start', 'end', 'details', 'details_plaintext', 'details_plaintext_type', 'task')
        fields = '__all__'
