from django.urls import re_path
from rest_framework.routers import DefaultRouter
from rest_framework.documentation import include_docs_urls

from . import views

router = DefaultRouter()
router.register(r'api/task', views.TaskAPI, base_name='task')
router.register(r'api/progress', views.TaskProgressAPI, base_name='progress')

urlpatterns = [] + router.urls + [
    re_path(r'^docs/', include_docs_urls(title='Task API'))
]
