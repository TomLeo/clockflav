from django.db import models

from django.db import models
from django.contrib.auth.models import User
import humanize

MD = 'md'
RST = 'rst'
TXT = 'txt'
PLAIN_TEXT_TYPES = (
    (MD, 'markdown'),
    (RST, 'reStructuredText'),
    (TXT, 'text')
)

OPEN = 'open'
CLOSED = 'closed'
MERGED = 'merged'
MERGE_STATUS_TYPES = (
    (OPEN, 'Open'),
    (CLOSED, 'Closed'),
    (MERGED, 'Merged')
)


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    git_branch = models.CharField(max_length=512, blank=True)
    pull_request_url = models.CharField(max_length=512, blank=True)
    merge_status = models.CharField(max_length=64, choices=MERGE_STATUS_TYPES, default=OPEN, blank=True)
    time_estimate = models.FloatField(blank=True, null=True, verbose_name='Number of estimated minutes')
    # todos = models.TextField(blank=True, default='')  # TODO: json field [{is_checked: False, todo_name: ''}, ...]
    # tags = models.ManyToManyField()  TODO: tag/label/category app?
    complete = models.BooleanField(default=False)

    @property
    def duration(self):
        result_tmpl = "%s"
        if self.taskprogress_set.filter(end__isnull=True).exists():
            result_tmpl = "In progress %s so far"
        so_far = sum([i.duration.total_seconds() for i in self.taskprogress_set.filter(end__isnull=False)])
        result = result_tmpl % humanize.naturaldelta(so_far)
        return result


class TaskProgress(models.Model):

    start = models.DateTimeField()
    end = models.DateTimeField(blank=True, null=True)
    details = models.TextField(blank=True, default='')
    details_plaintext = models.TextField(blank=True, default='')
    details_plaintext_type = models.CharField(max_length=8, blank=True, choices=PLAIN_TEXT_TYPES, default=TXT)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    # TODO: related_tasks = models.ManyToManyField(Task)

    @property
    def duration(self):
        if self.start and self.end:
            return self.end - self.start

    @property
    def get_duration_display(self):
        _duration = self.duration
        if not _duration:
            return "In progress"
        return humanize.naturaldelta(_duration)

    def __str__(self):
        return self.get_duration_display
