import logging

from rest_framework import viewsets

from tasks.models import Task, TaskProgress
from tasks.serializers import TaskSerializer, TaskProgressSerializer

logger = logging.getLogger(__name__)

class TaskAPI(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

class TaskProgressAPI(viewsets.ModelViewSet):
    queryset = TaskProgress.objects.all()
    serializer_class = TaskProgressSerializer
