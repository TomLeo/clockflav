from django.contrib import admin

from tasks.models import Task, TaskProgress

class TaskProgressInline(admin.StackedInline):
    model = TaskProgress
    fields = ['start', 'end', 'details_plaintext', 'details_plaintext_type', 'task']


class TaskProgressAdmin(admin.ModelAdmin):
    list_display = ('get_duration_display',)


class TaskAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',), }
    list_display = ('name', 'duration', 'git_branch', 'pull_request_url', 'merge_status', 'time_estimate')
    inlines = [TaskProgressInline]


admin.site.register(Task, TaskAdmin)
admin.site.register(TaskProgress, TaskProgressAdmin)
