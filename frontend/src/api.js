import axios from 'axios';

const REST_REQ_LIST = 'list';
const REST_REQ_CREATE = 'create';
const REST_REQ_READ = 'read';
const REST_REQ_UPDATE = 'update';
const REST_REQ_PARTIAL_UPDATE = 'partial_update';
const REST_REQ_DELETE = 'delete';

const protocol = process.env.VUE_APP_PROTOCOL;
const host = process.env.VUE_APP_HOST;
const port = process.env.VUE_APP_PORT;
const basePath = process.env.VUE_APP_BASEPATH;

const taskAPI = (function(protocol, host, port, basePath) {
    let baseUrl = `${protocol}://${host}`;
    if (port) {
        baseUrl += `:${port}`;
    }
    baseUrl += '/' + basePath + '/';
    return axios.create({
        baseURL: baseUrl,
        timeout: 1000,
    });
})(protocol, host, port, basePath);

const ENDPOINT_TASK = 'task';
const ENDPOINT_PROGRESS = 'progress';

const API = {
    [ENDPOINT_TASK]: {
        [REST_REQ_LIST]: () => taskAPI.get(ENDPOINT_TASK),
        [REST_REQ_CREATE]: (data) => taskAPI.post(ENDPOINT_TASK+'/', data),
        [REST_REQ_READ]: (id) => {
            const url = `${ENDPOINT_TASK}/${id}`;
            return taskAPI.get(url);
        },
        [REST_REQ_UPDATE]: (id, data) => {
            const url = `${ENDPOINT_TASK}/${id}`;
            return taskAPI.put(url, data);
        },
        [REST_REQ_PARTIAL_UPDATE]: (id, data) => {
            const url = `${ENDPOINT_TASK}/${id}`;
            return taskAPI.patch(url, data);
        },
        [REST_REQ_DELETE]: (id) => {
            const url = `${ENDPOINT_TASK}/${id}`;
            return taskAPI.delete(url, data);
        },
    },
    [ENDPOINT_PROGRESS]: {
        [REST_REQ_LIST]: () => taskAPI.get(ENDPOINT_PROGRESS),
        [REST_REQ_CREATE]: (data) => taskAPI.post(ENDPOINT_PROGRESS, data),
        [REST_REQ_READ]: (id) => {
            const url = `${ENDPOINT_PROGRESS}/${id}`;
            return taskAPI.get(url);
        },
        [REST_REQ_UPDATE]: (id, data) => {
            const url = `${ENDPOINT_PROGRESS}/${id}`;
            return taskAPI.put(url, data);
        },
        [REST_REQ_PARTIAL_UPDATE]: (id, data) => {
            const url = `${ENDPOINT_PROGRESS}/${id}`;
            return taskAPI.patch(url, data);
        },
        [REST_REQ_DELETE]: (id) => {
            const url = `${ENDPOINT_PROGRESS}/${id}`;
            return taskAPI.delete(url, data);
        },
    }
};

export default API;