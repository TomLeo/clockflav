import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { MdButton } from 'vue-material/dist/components'

Vue.use(MdButton)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
