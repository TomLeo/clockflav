export default function clone(thing) {
	return JSON.parse(JSON.stringify(thing));
}
