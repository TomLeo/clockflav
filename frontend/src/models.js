const fieldTypes = {
	_isNumber(thing) {
		return typeof(thing) === 'number';
	},
	isString(thing) {
		return typeof(thing) === 'string';
	},
	isBoolean(thing) {
		return typeof(thing) === 'boolean';
	},
	isInteger(thing) {
		return this._isNumber(thing) && parseInt(thing) === thing;
	},
	isFloat(thing) {
		return this._isNumber(thing) && (""+thing).includes(".")
	},
	isDateTime(thing) {
		return isString(thing) && new Date(thing) != "Invalid Date"
	},
	isNull(thing) {
		return thing === null;
	},
	isDateTimeOrNull(thing) {
		return isNull(thing) || isDateTime(thing);
	},
}

function validateShape(shape, obj) {
	const inValidFields = Object.keys(shape).filter((key) => {
		const validator = shape[key];
		return validator(obj[key]);
	});
	const isValid = (inValidFields.length === 0);
	return ([
		isValid,
		inValidFields,
	]);
}

const progressObject = {
	shape: {
        id: fieldTypes.isInteger,
        start: fieldTypes.isDateTime,
        end: fieldTypes.isDateTimeOrNull,
        details: fieldTypes.isString,
        details_plaintext: fieldTypes.isString,
        details_plaintext_type: fieldTypes.isString,
        task: fieldTypes.isInteger,
    },
   	isValid(obj) {
   		return validateShape(this.shape, obj);
   	},
   	blankObj() {
   		return {
	   		start: new Date(),
	   		end: null,
	   		details: '',
	   		details_plaintext: '',
	   		details_plaintext_type: 'txt',
	   		task: null
   		};
   	},
};

const taskObject = {
	shape: {
	    id: fieldTypes.isInteger,
	    name: fieldTypes.isString,
	    slug: fieldTypes.isString,
	    git_branch: fieldTypes.isString,
	    pull_request_url: fieldTypes.isString,
	    merge_status: fieldTypes.isString,
	    time_estimate: fieldTypes.isFloat,
	    complete: fieldTypes.isBoolean,
	    user: fieldTypes.isInteger,
	},
	isValid(obj) {
		return validateShape(this.shape, obj);
	},
	blankObj() {
		return {
			name: '',
			slug: '',
			git_branch: '',
			pull_request_url: '',
			merge_status: '',
			time_estimate: 0.0,
			complete: false,
			user: null
		};
	},
};

const models = {
	progress: progressObject,
	task: taskObject,
}

export default models;
