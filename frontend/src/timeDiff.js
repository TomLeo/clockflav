export default function timeDiff(d1, d2) {
    let diff = d2 - d1,
        sign = (diff<0?-1:1),
        milliseconds,
        seconds,
        minutes,
        hours,
        days;
    diff = Math.abs(diff);
    diff = (diff-(milliseconds=diff%1000))/1000;
    diff = (diff-(seconds=diff%60))/60;
    diff = (diff-(minutes=diff%60))/60;
    days = (diff-(hours=diff%24))/24;
    return {
        sign: (sign === 1 ? "elapsed": "remains"),
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds,
        milliseconds: milliseconds,
    }
}
