import Vue from 'vue'
import Vuex from 'vuex'
import API from '@/api.js'

Vue.use(Vuex)

const state = {

}

const mutations = {

}

const actions = {
	createTask({commit}, data) {
		data.slug = data.name;
		data.user = 1;
		return new Promise(function(resolve, reject) {
			API.task.create(data).then(
				function(resp) {
					if (resp.status !== 201) {
						reject(resp.statusText);
					}
					console.log(resp.data);
					resolve(resp.data);
				},
				function(reason) {
					console.log(reason);
					reject(reason);
				}
			);
		});
	},
}

export default new Vuex.Store({
  state,
  mutations,
  actions,
})
